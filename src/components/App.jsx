import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import NavigationBar from './NavigationBar/NavigationBar';
import Routes from '../AppRoutes';


class App extends React.Component {
  componentWillMount() {
    // this.context.router.history.push("/login");
  }

  render() {

    return (
      <div className="main-container">
        <div>
          <NavigationBar />


          <Routes />
        </div>
      </div>
    );
  }
}


export default App;
