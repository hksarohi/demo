import React from 'react';
import './FilterHOC.less';

const listFilters = (Component, filters, props) => Object.keys(filters).map((datum, index) => (

  <Component data={datum} filters={filters} {...props} key={index} />
));

export default Component => (props) => {
  const { filters } = props;

  if (!filters || Object.keys(filters).length <= 0) {
    return null;
  }

  return (
    <div className="filterWrapper">
      {listFilters(Component, filters, props)}

    </div>
  );
};
