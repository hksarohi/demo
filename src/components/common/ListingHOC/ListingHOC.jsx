import React from 'react';
import Loader from '../../Loader/Loader';
import './ListingHOC.less';

const listElements = (Component, data, props) => data.map((datum, index) => (
  <div className="item" key={datum.id}>
    <Component data={datum} {...props} />
  </div>
));

export default Component => (props) => {
  const { isHidden, listData, isLoading } = props;

  if (isLoading) {
    return (<Loader />);
  }

  if (isHidden || !listData || !Array.isArray(listData) || listData.length <= 0) {
    return null;
  }


  return (
    <div className="itemsWrapper">
      {listElements(Component, listData, props)}
    </div>
  );
};
