/**
 * Created by Harsh on 24/02/19.
 */
import React, { Component } from 'react';
import './ImageWrapper.less';

class ImageWrapper extends Component {
  constructor(props) {
    super(props);
    this.ironImageHd = React.createRef();
    this.preloadImage = React.createRef();
  }

  componentDidMount() {
    const hdLoaderImg = new Image();
    const { srcLoaded } = this.props;
    hdLoaderImg.src = srcLoaded;


    hdLoaderImg.onload = () => {
      this.ironImageHd.current.setAttribute(
        'src',
        `${srcLoaded}`,
      );
      this.ironImageHd.current.classList.add('image-fade-in');
      this.preloadImage.current.classList.add('hide-preload');
    };
  }

  render() {
    return (
      <div className="image-container">

        <img
          alt="Image"
          className="image-loaded"
          ref={this.ironImageHd}
        />
        <img
          alt="Placeholder Image"
          className="image-preload"
          src={this.props.srcPreload}
          ref={this.preloadImage}
        />

      </div>
    );
  }
}

export default ImageWrapper;
