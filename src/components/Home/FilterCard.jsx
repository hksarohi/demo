/**
 * Created by harshsarohi on 24/02/19.
 */
/**
 * Created by harshsarohi on 24/02/19.
 */
import React, { Fragment } from 'react';
import './FilterCard.less';


const FilterCard = (props) => {
  const { getData, filters, data } = props;
  if (!data) {
    return null;
  }


  const activeClass = (filters[data] ? ' active' : '');

  return (
    <Fragment>
      <div className={`filter ${activeClass}`} onClick={() => getData(data)}>

        {data}

      </div>

    </Fragment>
  );
};

export default FilterCard;
