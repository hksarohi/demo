
import React from 'react';

export default (Component, headingText) => (props) => {
  const { isHidden, altHeading } = props;

  if (isHidden) {
    return null;
  }
  const heading = headingText || altHeading;
  return (
    <div>
      <h2>{heading}</h2>
      <Component {...props} />
    </div>
  );
};
