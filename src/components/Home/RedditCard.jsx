/**
 * Created by harshsarohi on 24/02/19.
 */
import React, { Fragment } from 'react';
import ImageWrapper from '../common/ImageWrapper/ImageWrapper'
import  './RedditCard.less';


const image = 'https://i.ibb.co/KFLD3Xb/placeholder.png';


const RedditCard = (props) => {
    const { data } = props;
    if (!data) {
        return null;
    }






    return (
        <Fragment>
            <div className="card">
                <div className="card-ups">
                    <span className="arrow" />
                    <span className="text">{data.ups}</span>
                    <span className="arrow down" />
                </div>
                <div className="card-content">
                    <h2 className="title">{data.title}</h2>
                    <a href={data.url} target="_blank" className="externalLink">

                        <ImageWrapper srcPreload={image} srcLoaded={data.url} width="300" height="200"/>


                    </a>
                    <div className="comments">
                        <span className="arrow-box"></span>
                        <span>{data.num_comments}</span>
                    </div>
                </div>
            </div>
        </Fragment>
    );
};

export default RedditCard;
