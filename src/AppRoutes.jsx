import React from 'react';
import {
  Switch, Route, withRouter,
} from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Loader from './components/Loader/Loader';

import DynamicImport from './components/DynamicImport';


// import DynamicImport from "./Common/DynamicImport";


const HomePage = props => (
  <DynamicImport
    load={() => import('./components/Home/HomePage' /* webpackChunkName:"homePage" */)}
  >
    {Component => (
      Component === null
        ? <div className="center"><Loader  /></div>
        : <Component {...props} />
    )}
  </DynamicImport>
);


const Routes = props => (
  <Switch>
    <Route exact path="/" component={HomePage} title="Login" />
  </Switch>
);


Routes.contextTypes = {
  router: PropTypes.object,
};
function mapStateToProps(state) {
  return {
    auth: state.loginUser,
  };
}

export default withRouter(connect(mapStateToProps)(Routes));
