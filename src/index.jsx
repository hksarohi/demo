import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import { createStore, applyMiddleware, compose } from 'redux';
// import jwtDecode from 'jwt-decode';
import { BrowserRouter } from 'react-router-dom';
import rootReducer from './rootReducer';


import App from './components/App';

// import createLogger from 'redux-logger'
const isDeveloping = process.env.NODE_ENV !== 'production';

// const logger = createLogger();
let store;
const middlewares = [
  thunk,
];

const enhancers = [
  applyMiddleware(...middlewares),
];

store = createStore(
  rootReducer, compose(
        ...enhancers
    )
);


render(
  <Provider store={store}>
    <BrowserRouter basename="/">
      <App />
    </BrowserRouter>
  </Provider>,
  document.getElementById('app'),
);
