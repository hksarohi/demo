import {
  REDDIT_RESET_STORE,
  REDDIT_REQUEST_INIT,
  REDDIT_REQUEST_SUCCESS,
  REDDIT_REQUEST_FAILED,

} from './types';

import Request from '../utils/Request';
import urls from '../utils/urls';
/**
 * Login Actions
 */

export function redditResetStore() {
  return {
    type: REDDIT_RESET_STORE,
  };
}

export function redditInit() {
  return {
    type: REDDIT_REQUEST_INIT,
  };
}

export function redditSuccessful(data, headers) {
  return {
    type: REDDIT_REQUEST_SUCCESS,
    payload: {
      receivedAt: Date.now(),
      data,
      headers,
    },
  };
}

export function redditFailed() {
  return {
    type: REDDIT_REQUEST_FAILED,
  };
}

export function redditRequest(subreddit) {
  console.log(subreddit);

  return (dispatch) => {
    const successFn = (data, headers) => {
      dispatch(redditSuccessful(data, headers));
    };

    const errorFn = (error) => {
      console.log('error');
      dispatch(redditFailed(error));
    };

    dispatch(redditInit());

    const api = new Request(dispatch, successFn, errorFn);
    const url = urls.reddit.REDDIT_REQUEST
      .replace('{subreddit}', subreddit);

    return api.get(url);
  };
}
