import {
  REDDIT_RESET_STORE,
  REDDIT_REQUEST_INIT,
  REDDIT_REQUEST_SUCCESS,
  REDDIT_REQUEST_FAILED,

} from '../actions/types';
import {
  getObjectValueIfEmpty, clone,
} from '../utils/common';

const initialState = {
  /*
   * TODO- isAuthenticated and user data need to be fetched from local storage
   */
  isLoading: false,
  isLoaded: false,
  redditData: null,
};

export default (state = initialState, action = {}) => {
  let changes = {};

  switch (action.type) {
    case REDDIT_RESET_STORE:
      changes = { ...initialState };
      break;

    case REDDIT_REQUEST_INIT:
      changes = {
        isLoading: true,
        isLoaded: false,
        redditData: null,
      };
      break;

    case REDDIT_REQUEST_SUCCESS: {
      const redditData = getObjectValueIfEmpty(action, 'payload.data.data', {});
      changes = {
        isLoading: false,
        isLoaded: true,
        redditData,
      };
    }
      break;

    case REDDIT_REQUEST_FAILED:
      changes = {
        isLoading: false,
        isLoaded: true,
        redditData: null,
      };
      break;

    default:
      changes = {};
      break;
  }

  return clone(state, changes);
};
