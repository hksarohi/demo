module.exports = [
  {
    test:/\.(js|jsx)$/,
    exclude: /(node_modules|bower_components|public\/)/,
    loader: 'babel-loader',
    options: {
      presets: ['react', 'es2015', 'stage-1'],
        plugins: [

            'syntax-dynamic-import',
            "transform-runtime"

        ]
    }
  },
  {
    test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
    exclude: /(node_modules|bower_components)/,
    loader: 'file-loader',
  },
  {
    test: /\.(woff|woff2)$/,
    exclude: /(node_modules|bower_components)/,
    loader: 'file-loader?prefix=font/&limit=5000',
  },
  {
    test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
    exclude: /(node_modules|bower_components)/,
    loader: 'file-loader?limit=10000&mimetype=application/octet-stream',
  },
  {
    test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
    exclude: /(node_modules|bower_components)/,
    loader: 'file-loader?limit=10000&mimetype=image/svg+xml',
  },
  {
    test: /\.gif/,
    exclude: /(node_modules|bower_components)/,
    loader: 'file-loader?limit=10000&mimetype=image/gif',
  },
  {
    test: /\.jpg/,
    exclude: /(node_modules|bower_components)/,
    loader: 'file-loader?limit=10000&mimetype=image/jpg',
  },
  {
    test: /\.png/,
    exclude: /(node_modules|bower_components)/,
    loader: 'file-loader?limit=10000&mimetype=image/png',
  },
  {
    test: /\.(jpe?g|png|gif|svg)$/i,
    loaders: [
      'file-loader?hash=sha512&digest=hex&name=[hash].[ext]',
      'image-webpack-loader'
    ]
  },
  {
    test: /\.less$/,
    loader: 'style-loader!css-loader!less-loader'
  },
    {
        test: /\.css$/,
        loader: 'style-loader!css-loader!less-loader'
    }
];
