const webpack = require('webpack');
const path = require('path');
const loaders = require('./webpack.loaders.js');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const WebpackCleanupPlugin = require('webpack-cleanup-plugin');

module.exports = {
    entry: [
        './client/index.js',
    ],
    output: {
        path: path.resolve(__dirname, 'public/build'),
        filename: '[name]-[hash].min.js',
        publicPath: '/admin/coupon',
    },
    resolve: {
        extensions: ['.js', '.jsx'],
    },
    module: {
        loaders,
    },
    plugins: [
        new WebpackCleanupPlugin(),
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: JSON.stringify('production'),
            },
        }),
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false,
                screw_ie8: true,
                drop_console: true,
                drop_debugger: true,
            },
        }),
        new webpack.optimize.OccurrenceOrderPlugin(),
        new HtmlWebpackPlugin({
            template: path.join(__dirname, 'public', 'index.html'),
            inject: 'body',
        }),
    ],
};